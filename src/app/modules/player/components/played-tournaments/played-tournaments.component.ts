import { Component, OnInit } from '@angular/core';
import { IPlayedTournaments } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-played-tournaments',
  templateUrl: './played-tournaments.component.html',
  styleUrls: ['./played-tournaments.component.scss']
})
export class PlayedTournamentsComponent implements OnInit {

  constructor(private playerData:PlayerDataService) { }
  playedTounaments!:IPlayedTournaments[]
  ngOnInit(): void {
    this.playerData.getPlayedTournaments().subscribe({
      next:(response:IPlayedTournaments[])=>{
        this.playedTounaments=response;
        console.log(this.playedTounaments);
      },
      error:(error:any)=>{
        console.log(error.message);
      }
      
    })
  }

}
