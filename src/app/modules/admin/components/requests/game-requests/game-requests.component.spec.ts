import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameRequestsComponent } from './game-requests.component';

describe('GameRequestsComponent', () => {
  let component: GameRequestsComponent;
  let fixture: ComponentFixture<GameRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameRequestsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GameRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
