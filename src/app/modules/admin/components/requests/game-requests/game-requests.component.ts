import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ITournament } from 'src/app/types';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-game-requests',
  templateUrl: './game-requests.component.html',
  styleUrls: ['./game-requests.component.scss']
})
export class GameRequestsComponent implements OnInit {

  constructor(private data:DataService,private snackBar:MatSnackBar) { }

  pendingRequests!:ITournament[];
  ngOnInit(): void {
    this.data.getPendingGameRequests().subscribe({
      next:(response)=>{
        this.pendingRequests=response;
        console.log(this.pendingRequests)
      }
    })

  }

  acceptRequest(gameId:number){
    const details={
      tournamentId:gameId,
      status:"APPROVED"
    }
    this.data.acceptOrRejectGameRequest(details).subscribe({
      next:()=>{
        this.snackBar.open("Request Approved");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }

  rejectRequest(gameId:number){
    const details={
      tournamentId:gameId,
      status:"REJECTED"
    }
    this.data.acceptOrRejectGameRequest(details).subscribe({
      next:()=>{
        this.snackBar.open("Request Rejected");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }

}
