import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ITournament } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-my-tournaments',
  templateUrl: './my-tournaments.component.html',
  styleUrls: ['./my-tournaments.component.scss']
})
export class MyTournamentsComponent implements OnInit {

  constructor(private playerData:PlayerDataService,private snackBar:MatSnackBar) { }

  myTournaments!:ITournament[]
  ngOnInit(): void {
    this.playerData.getMyTournaments().subscribe({
      next:(response)=>{
        this.myTournaments=response;
        console.log(this.myTournaments);
      },
      error:(error)=>{
        console.log(error.message);
      }
    })
  }

  stopTournament(tournamentId:any){
    this.playerData.stopMyTournament(tournamentId).subscribe({
      next:()=>{
        this.snackBar.open("Tournament Has Been Stoped for now");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }
deleteTournament(gameId:number){
  this.playerData.deleteTournament(gameId).subscribe({
    next:()=>{
      setTimeout(()=>{
        this.ngOnInit()
      },300)
    }
  });
}

}
