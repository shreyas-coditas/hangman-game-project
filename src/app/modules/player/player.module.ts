import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayerRoutingModule } from './player-routing.module';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { SidebarComponent } from '../shared/components/sidebar/sidebar.component';
import { AllTournamentsComponent } from './components/all-tournaments/all-tournaments.component';
import { MyTournamentsComponent } from './components/my-tournaments/my-tournaments.component';
import { PlayedTournamentsComponent } from './components/played-tournaments/played-tournaments.component';
import { TournamentDetailsComponent } from './components/tournament-details/tournament-details.component';
import {  GamePageComponent } from './components/game-page/game-page.component';
import { QuestionsComponent } from './components/game-page/questions/questions.component';

import { KeyboardComponent } from './components/game-page/keyboard/keyboard.component';

import { ProfileComponent } from './components/profile/profile.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { CreateTournamentComponent } from './components/create-tournament/create-tournament.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { HttpClientModule } from '@angular/common/http';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
// import { ChartsModule } from 'ng2-charts';
// import { FormatTimePipe } from 'src/app/pipes/format-time.pipe';
// import { FormatTimePipe } from './components/game-page/game-page.component';
@NgModule({
  declarations: [
    HomeComponent,
    AllTournamentsComponent,
    MyTournamentsComponent,
    PlayedTournamentsComponent,
    TournamentDetailsComponent,
    GamePageComponent,
    QuestionsComponent,
    KeyboardComponent,
   
    ProfileComponent,
    CreateTournamentComponent,
    LeaderboardComponent,
   
  ],
  imports: [
    CommonModule,
    PlayerRoutingModule,
    SharedModule,
    HighchartsChartModule,
    ReactiveFormsModule,
    // ChartsModule,,
    FormsModule,
   HttpClientModule
  ],
  providers:[
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}}
  ]
})
export class PlayerModule { }
