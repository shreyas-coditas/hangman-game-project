import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayedTournamentsComponent } from './played-tournaments.component';

describe('PlayedTournamentsComponent', () => {
  let component: PlayedTournamentsComponent;
  let fixture: ComponentFixture<PlayedTournamentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayedTournamentsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlayedTournamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
