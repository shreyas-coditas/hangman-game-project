import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { PlayersComponent } from './components/players/players.component';
import { ProductsComponent } from './components/products/products.component';
import { GameRequestsComponent } from './components/requests/game-requests/game-requests.component';
import { RedeemRequestsComponent } from './components/requests/redeem-requests/redeem-requests.component';
import { RequestsComponent } from './components/requests/requests.component';

const routes: Routes = [
  {path:'',component:AdminDashboardComponent,children:[
    {path:'players',component:PlayersComponent},
    {path:'categories',component:CategoriesComponent},
    {path:'products',component:ProductsComponent},
    {path:'requests',component:RequestsComponent,children:[
      {path:'game-requests',component:GameRequestsComponent},
      {path:'redeem-requests',component:RedeemRequestsComponent}
    ]},
    {path:'leaderboard',component:LeaderboardComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
