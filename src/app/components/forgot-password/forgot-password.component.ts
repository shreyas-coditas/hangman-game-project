import { Component, OnInit } from '@angular/core';
import { FormBuilder ,Validators} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private formBuilder:FormBuilder
    ,private auth:AuthService
    ,private snackBar:MatSnackBar,
    private router:Router) { }

  ngOnInit(): void {
  }
  forgotPassword=this.formBuilder.group({
    "email":['',[Validators.required,Validators.email]],
    "otp":['',Validators.required],
    "newPassWord":['',Validators.required]
  })


  onSubmit(){
    console.log(this.forgotPassword)
    this.auth.verifyOtp(this.forgotPassword.value).subscribe({
      next:()=>{
        this.snackBar.open("your password has been changed succesfully")
        this.router.navigate(['']);
      }
    })
  }
}
