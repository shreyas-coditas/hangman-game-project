import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnInit {

  formFields!: any;
  form!: FormGroup;
  description: string;

  @Output() formEmitter = new EventEmitter();
  ngOnInit(): void {
    
  }
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModalFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.description = data.description;
    this.form = data.form;
    this.formFields = data.fields;


  }
  onSubmit() {
    this.dialogRef.close();
  }

  close() {
    this.dialogRef.close();
  }
}
