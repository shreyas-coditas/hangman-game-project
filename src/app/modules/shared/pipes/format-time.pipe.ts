import { Pipe, PipeTransform } from '@angular/core';



  @Pipe({
    name: 'formatTime',
  })
  export class FormatTimePipe implements PipeTransform {
    transform(value: number): string {
      let minutes: number = Math.floor(value / 60);
      let hours:number=0
      if(minutes>=60){
        hours=Math.floor(minutes / 60)
        minutes=minutes-hours*60;
      }
      return (
        ('00'+ hours).slice(-2)+
        ':'+
        ('00' + minutes).slice(-2) +
        ':' +
        ('00' + Math.floor(value - minutes * 60)).slice(-2)
      );
    }
  }

