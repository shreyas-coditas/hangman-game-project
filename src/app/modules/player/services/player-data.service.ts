import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICategory, IGift, IPlayedTournaments, IProfile, IprogressData, IQuestion, IResult, ITournament } from 'src/app/types';

@Injectable({
  providedIn: 'root'
})
export class PlayerDataService {

  constructor(private http:HttpClient) { }
  headers={
    "ngrok-skip-browser-warning":"1234", 
  }

  playerId=localStorage.getItem('id');
  resetPassword(resetCredentials:any){
    return this.http.post('api/reset',resetCredentials)
  }
  getCategories(){
    return this.http.get<ICategory[]>('getCategoryList')
  }
  addTournament(id:any,tounamentDetails:any){
    const playerId=parseInt(id);
    return this.http.post(`api/player/createTournament/${playerId}`,tounamentDetails)
  }
  getAllTournaments(data:any){
    let url=''
    
    if(data!==null){
      url=`getAllTournaments?categoryValues=${data.categories}&pageSize=${data.pageSize}&sortBy=${data.sortBy}&pageNo=${data.pageNum}&sortDir=${data.sortOrder}`
    }
    // if(data.categories){

    // }
    // if(data!==null){
    //   url=`getAllTournaments?${categoryValues=data.categories?data.categories}&pageSize=${data.pageSize}&sortBy=${data.sortBy}&pageNo=${data.pageNum}&sortDir=${data.sortOrder}`
    // }
    else{
      url='getAllTournaments'
    }
    console.log(url);
    const playerId=localStorage.getItem('id');
    return this.http.get<ITournament[]>(url)
  }

  getMyTournaments(){
    const playerId=localStorage.getItem('id');
    return this.http.get<ITournament[]>(`api/player/selfCreated/${playerId}`)
  }
  getQuestions(id:number){
    return this.http.get(`questionSet/${id}`)
  }

  getProfileDetails(){
    const playerId=localStorage.getItem('id');
    return this.http.get<IProfile>(`api/player/getSelfInfo/${playerId}`)
  }

  getProgress(){
    const playerId=localStorage.getItem('id');
    return this.http.get<IprogressData>(`api/player/graph/${playerId}`)
  }
  // getQuestionsSet(id:number){
  //   return this.http.get<IQuestion[]>(`questionSet/${id}`)
  // }

  stopMyTournament(gameId:number){
    const data={
      "tournamentId":gameId,
      "playerId":this.playerId
    }
    return this.http.post('api/player/stopTournament',data);
  }
  deleteTournament(gameId:number){
    return this.http.put(`api/player/delete/${gameId}`,{})
  }

  addDataToLeaderboard(data:IResult){
    return this.http.post('api/addLeadBoard',data);
  }
  getDataForLeaderboard(tournamentId:number){
    return this.http.get<IResult[]>(`api/getScoresForTournament/${tournamentId}`)
  }

  getAllGifts(){
    return this.http.get<IGift[]>('gifts/getGiftList');
  }

  redeemMygift(giftDetails:any){
    console.log(giftDetails);
    return this.http.post(`request/addGiftRequest`,giftDetails)
  }
  getPlayedTournaments(){
    const playerId=localStorage.getItem('id');
    return this.http.get<IPlayedTournaments[]>(`api/player/getLeadBoardForParticularPlayer/${playerId}`)
  }

  logOutTime(duration:number){
    const data={
      "playerId":this.playerId,
      "duration":duration
    }
    return this.http.post(`api/player/logout`,data)
  }
}
