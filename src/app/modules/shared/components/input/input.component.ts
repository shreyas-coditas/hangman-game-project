import { Component, EventEmitter, forwardRef, Input, OnInit, Output, Provider } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
const Input_CONTROL_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputComponent),
  multi: true,
};
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [Input_CONTROL_VALUE_ACCESSOR]
})
export class InputComponent implements ControlValueAccessor{
  InputValue:any
  constructor() { }
  @Input() type = 'text'
  @Input() control!:string;
  @Input() fromControl!:FormControl;
  @Input() placeholder!:string
  @Input() label!:string;
  @Output() onInput=new EventEmitter();
  onChange = (value: any) => {};
  onTouch = () => {};
  @Output() OnInput(event:Event){
    this.writeValue((<HTMLInputElement>event.target).value)
    this.InputValue=(<HTMLInputElement>event.target).value;
  }
  writeValue(value: any): void {
    this.onChange(value);
    this.InputValue = value;
  }
  registerOnChange(fn: (value:any) => void): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }
}