import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent implements OnInit {

  constructor() { }
  @Output() keyPress=new EventEmitter()
  ngOnInit(): void {
  }
  keys=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];


  onPress(key:string){
    console.log(key)
    this.keyPress.emit(key);
  }
}
