import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit {
  

  constructor(@Inject(MAT_DIALOG_DATA) public data:any,private dialogRef: MatDialogRef<DialogBoxComponent>) { }
  @Input() modalTitle!:string;
  @Input() form!:FormGroup;
  @Input() formFields!:any;

  
  ngOnInit(): void {
  }
  // close() {
  //   this.dialogRef.close();
  // }
}
