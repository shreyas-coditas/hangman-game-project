import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IResult } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private playerData:PlayerDataService
    ) { }

    gameId!:number;
    leaderBoardDetails!:IResult[];
  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      console.log(params)
      this.gameId = params['tournamentId'];
      console.log(this.gameId)
    })

    
    this.playerData.getDataForLeaderboard(this.gameId).subscribe({
      next:(response:IResult[])=>{
        this.leaderBoardDetails=response;
        console.log(response);
      },
      error:(error)=>{
        console.log(error.message);
      },
      complete:()=>{
        
      }
    })
    

  }

  goToAllTournaments(){
    this.router.navigate(['player','all-tournaments']);
  }
}
