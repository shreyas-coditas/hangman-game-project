import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { SignUpLoginComponent } from './components/sign-up-login/sign-up-login.component';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { HomeComponent } from './modules/player/home/home.component';

const routes: Routes = [
  {path:'',component:SignUpLoginComponent},
  {path:'forgot',component:ForgotPasswordComponent},
  {path:'player', loadChildren: () => import('./modules/player/player.module').then(n => n.PlayerModule),canActivate: [AuthGuard, RoleGuard], data: { role: 'player' }},
  // canActivate: [AuthGuard, RoleGuard], data: { role: 'player' }},
  {path:'admin', loadChildren: () => import('./modules//admin/admin.module').then(n => n.AdminModule),canActivate: [AuthGuard, RoleGuard], data: { role: 'admin' }},
  // canActivate: [AuthGuard, RoleGuard], data: { role: 'admin' }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
