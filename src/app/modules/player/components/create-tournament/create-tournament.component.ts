import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray,FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ICategory } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-create-tournament',
  templateUrl: './create-tournament.component.html',
  styleUrls: ['./create-tournament.component.scss']
})
export class CreateTournamentComponent implements OnInit {

  constructor(
    private formBuilder:FormBuilder,
    private playerData:PlayerDataService,
    private snackBar:MatSnackBar
    ) { }

    categoriesList!:ICategory[];
  ngOnInit(): void {
    this.playerData.getCategories().subscribe({
      next:(response:ICategory[])=>{
        this.categoriesList=response;
      }
    })

  }
  // categories=new FormControl([]);
  itemsList=[
    {id:1,name:'car'},
    {id:2,name:'school'},
    {id:3,name:'food'},
    {id:4,name:'truck'},
    {id:5,name:'bike'}
  ]
  categories: FormControl = new FormControl([]);
  createGameForm = this.formBuilder.group({
    tournamentName:[''],
    firstPriceCoins:[''],
    secondPriceCoins:[''],
    thirdPriceCoins:[''],
    
    categoryList:this.categories,
    wordDtoList:this.formBuilder.array([
      this.formBuilder.group({
        wordName: '',
        hintName:'',
       
      })
    ]),
    
  });
  getControls():any{
    return (<FormArray> this.createGameForm.get('wordDtoList')).controls
  }
  addQuestion(): void {
    (this.createGameForm.get('wordDtoList') as FormArray).push(
      this.createItem()
    );
  }

  removeQuestion(index:any) {
    (this.createGameForm.get('wordDtoList') as FormArray).removeAt(index);
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      wordName: '',
      hintName:'',
      // hint2:'',
      // hint3:''
    });
  }
questions!:FormArray;

   getQuestions() {
    
    this.questions = this.createGameForm.get('wordDtoList') as FormArray;
    this.questions.push(this.createItem());
  }
  onSubmit(data:any){
    console.log(data)
    const newData={
      "tournamentName": data.tournamentName,
      "firstPriceCoins": data.firstPriceCoins,
      "secondPriceCoins": data.secondPriceCoins,
      "thirdPriceCoins": data.thirdPriceCoins,
      "categoryList":data.categoryList,
      "wordDtoList":data.wordDtoList
    }
    console.log(newData)
    let id=localStorage.getItem('id');
    this.playerData.addTournament(id,newData).subscribe({
      next:()=>{

      },
      error:(error)=>{
        console.log(error.message)
      },
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
        this.snackBar.open("Tournament added succesfully")
      }
    });
  }
  editDoctorFormFileds = [
    { label: "Name", inputType: "text", placeholder: "Enter Docotor Name", value: "", controlName: "doctorName" },
    { label: "Specialization", inputType: "text", placeholder: "Enter The Specialization", value: "", controlName: "specialization" },
  ]

}
