import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators ,FormControl,FormGroup} from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Router } from '@angular/router';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { AuthService } from 'src/app/services/auth.service';
import { IAuthResponse } from 'src/app/types';

@Component({
  selector: 'app-sign-up-login',
  templateUrl: './sign-up-login.component.html',
  styleUrls: ['./sign-up-login.component.scss']
})
export class SignUpLoginComponent implements OnInit {

  constructor(
    private formBuilder:FormBuilder,
    private auth:AuthService,
    private snackbar:MatSnackBar,
    private router:Router,
    private dialog:MatDialog,
   
    ) { }

  ngOnInit(): void {
    localStorage.setItem('token','');
    localStorage.setItem('role','');
    localStorage.setItem('id','');
  }
  loginCredentials=this.formBuilder.group({
    "userName":['',[Validators.required,Validators.email]],
    "password":['',Validators.required]
  })
  signUpCredentials=this.formBuilder.group({
    
    "userName":['',[Validators.required,Validators.email]],
    "userPassword":['',Validators.required],
    "playerName":['',Validators.required]
  })
  response!:IAuthResponse;
  onLogin(){
    console.log(this.loginCredentials);
    if (this.loginCredentials.valid) {
      this.auth.proceedLogin(this.loginCredentials.value).subscribe({
        next: (response:any) => {
          console.log(response);
          this.response=response;
        
          localStorage.setItem('token',response.accessToken);
          localStorage.setItem('role',response.tokenRole);
          localStorage.setItem('id',response.tokenUserID);
          
          if (this.response.tokenRole === 'admin') {
            this.router.navigate(['admin']);

          }
          else if(this.response.tokenRole === 'player'){
            this.router.navigate(['player']);

          }
          else{
            this.router.navigate(['']);
          }
          this.snackbar.open('Your Are Now Logged In')
        },
        error: (error) => {
          console.log(error.message);
          this.snackbar.open("Invalid Credentials");
        }
      })
  }
}
  onSignUp(){
    console.log(this.signUpCredentials);
    if(!this.signUpCredentials.valid){
      return;
    }
    // const password=this.signUpCredentials.value.password;
    // const data={
    //   email:this.signUpCredentials.value.email,
    //   password:Number(password),
    //   // returnSecureToken:true
    // }
    console.log(this.signUpCredentials);
    // console.log(data);
    this.auth.proceedSignUp(this.signUpCredentials.value).subscribe({
      next:(response:any)=>{ 
        console.log(response);
        this.snackbar.open("You Are Now Registered")
      },
      error:(error)=>{
        console.log(error.message);
        this.snackbar.open("Email Id Already Exists")
      },
      complete:()=>{
        console.log("Completed");
    }
    });
  }




  forgotPasswordForm = this.formBuilder.group({
    userName: ['', [Validators.required, Validators.email]],
   
  });


  forgotPasswordFormFileds = [
    { label: "Enter Username/Email", inputType: "email", placeholder: "Enter The Email", value: "", controlName: "userName" },
    
  ]

  openResetPasswordDialog = () => {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Reset Password',
      form: this.forgotPasswordForm,
      fields: this.forgotPasswordFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      
      this.getOpt(result.value);
      this.router.navigate(['forgot']);
      this.snackbar.open("Otp has been sent to your email");
      console.log(result);
    });

  }

  getOpt(changes:any){
    this.auth.getOtp(changes).subscribe({
      next:()=>{
        
        
      }
    });
  }
}
