import { coerceStringArray } from '@angular/cdk/coercion';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ICategory, ILeaderBoard } from 'src/app/types';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

  constructor(private formBuilder:FormBuilder,private data:DataService) { }
  categories!: ICategory[]
  ngOnInit(): void {
    this.data.getLeaderBoardDetails(null).subscribe({
      next:(response:ILeaderBoard[])=>{
        this.leaderboardData=response;
        this.leaderboardAttributes=Object.keys(response[0])
        console.log(this.leaderboardAttributes)
      }
    })

   
    this.data.getCategories().subscribe({
      next: (response) => {
        this.categories = response;
        console.log(this.categories);
        this.categoryString=this.categories[0].categoryName;
      }
    })
  }
  
  // tournaments!: ITournament[]
  selectedCategoriesList = new FormControl([]);
  sortByList!: string[]
  sortItem: string='score';
  categoryString: string = '';
  sortOrder: string = 'asc';
  pageSize: number = 5;
  pages = [3, 5, 10, 15, 20, 25]
  pageNum:number=0;
 
  searchValue:string=''
  leaderboardData!:ILeaderBoard[];
  leaderboardAttributes!:string[]
  search(){
    console.log(this.searchValue);
    this.categoryString=this.searchValue.toUpperCase();
    this.fetchLeaderbaord()
  }
 
  selectChange(event: any) {
    console.log(event.value);
    this.sortItem = event.value;
    this.fetchLeaderbaord()
  }
  selectPageSize(event: any) {
    this.pageSize = event.value;
    this.fetchLeaderbaord()
  }
  selectFilterCategory(event: any) {
    console.log(event.value)
    this.categoryString = event.value.join(',');
    console.log(this.categoryString)
    this.fetchLeaderbaord()
  }
  dscendingOrder() {
    this.sortOrder = 'desc'

    this.fetchLeaderbaord()
  }
  ascendingOrder() {
    this.sortOrder = 'asc'

    this.fetchLeaderbaord()
  }
  previous(){
    if(this.pageNum>0){
      this.pageNum--;
      this.fetchLeaderbaord()
    }
  }
  nextPage(){
    this.pageNum++;
    this.fetchLeaderbaord()
  }
  fetchLeaderbaord(){
    const configData={
      categories: this.categoryString,
      pageNum:this.pageNum,
      pageSize: this.pageSize,
      sortBy: this.sortItem,
      sortOrder: this.sortOrder
    }
    this.data.getLeaderBoardDetails(configData).subscribe({
      next:(response:ILeaderBoard[])=>{
        this.leaderboardData=response;
        
      }
    })
  }
}
