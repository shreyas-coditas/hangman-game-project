import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICategory, IGift, ILeaderBoard, IPlayedTournaments, IPlayer, IRedeemRequest, ITournament } from 'src/app/types';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }
  headers={
    "ngrok-skip-browser-warning":"1234", 
  }

 
 
  getPlayers(){
    return this.http.get<IPlayer[]>('getPlayers')
  }
  getCategoris(){
    return this.http.get<ICategory[]>('getCategoryList')
  }
  addCategory(details:any){
    return this.http.post('addCategory',details)
  }
  editCategoryDetails(id:number,details:any){
  
    return this.http.put(`api/updateCategory/${id}`,details)
  }
  deleteCategory(id:number){
    return this.http.delete(`deleteCategory/${id}`);
  }

  getGifts(){
    return this.http.get<IGift[]>('gifts/getGiftList')
  }
  addGift(details:any){
    return this.http.post('gifts/addGifts',details)
  }
  editGift(id:number,details:any){
  
    return this.http.patch(`gifts/updateGifts`,details)
  }
  deleteGift(id:number){
    return this.http.delete(`gifts/deleteGift/${id}`);
  }

  getPendingGameRequests(){
    return this.http.get<ITournament[]>('getPendingTournamentRequests')
  }

  acceptOrRejectGameRequest(details:any){
    
    return this.http.post(`approveTournamentRequest`,details);
  }
  getGiftRequests(){
    
    return this.http.get<IRedeemRequest[]>('request/getGiftRequests')
  }
  acceseptRedeemRequest(details:any){
    return this.http.post(`request/updateRequest`,details);
  }
  getCategories(){
    return this.http.get<ICategory[]>('getCategoryList')
  }
  getLeaderBoardDetails(config:any){
    let url=''
    if(config!==null){
      url=`api/getAllLeaderBoardList?pageSize=${config.pageSize}&sortBy=${config.sortBy}&pageNo=${config.pageNum}&sortDir=${config.sortOrder}&categoryValues=${config.categories}`
    }
    else{
      url='api/getAllLeaderBoardList'
    }
   
    return this.http.get<ILeaderBoard[]>(url);
  }
  
}
