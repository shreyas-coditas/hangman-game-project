import { Component, OnInit } from '@angular/core';
import { IPlayer } from 'src/app/types';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

  constructor(private data:DataService) { }
playerData!:IPlayer[]
  ngOnInit(): void {
    this.data.getPlayers().subscribe({
      next:(response:IPlayer[])=>{
        this.playerData=response;
        console.log(this.playerData)
      },
      error:(error)=>{
        console.log(error.message);
      }
    })

  }
  playerColumns: string[] = ['playerId', 'playerName', 'userName', 'gamesPlayed','totalCoins','lastLoginTime'];

  

 

}
