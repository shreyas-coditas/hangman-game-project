import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { ICategory } from 'src/app/types';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  constructor(
    private data:DataService,
    private formBuilder:FormBuilder,
    private dialog:MatDialog,
    private snackBar:MatSnackBar
    ) { }

  categories!:ICategory[]
  ngOnInit(): void {
    this.data.getCategoris().subscribe({
      next:(response:ICategory[])=>{
        this.categories=response;
      },
      error:(error)=>{
        console.log(error.message);
      }
    })

  }
  categoriesColumns: string[] = ['categoryId', 'categoryName','actions'];
 

 
 

  addCategoryForm = this.formBuilder.group({
    categoryName: ['', Validators.required],
    
  });


  addCategoryFormDetails = [
    { label: "Category Name", inputType: "text", placeholder: "Enter The Category", value: "", controlName: "categoryName" },
 
  ]

  openAddCategoryDialog = () => {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
     
      title: 'Add Category',
      form: this.addCategoryForm,
      fields: this.addCategoryFormDetails,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      
      // this.resetYourPassword(result.value);
      // this.snackBar.open("Your PassWord Has Been Reset");
      this.addNewCategory(result.value)
      console.log(result);
    });

  }

  addNewCategory(details:any){
    this.data.addCategory(details).subscribe({
      next:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
      }
    )
  }


  editCategoryForm = this.formBuilder.group({
    categoryName: ['', Validators.required],
    
  });


  editCategoryFormFileds = [
    { label: "Category Name", inputType: "text", placeholder: "", value: "", controlName: "categoryName" },
   
  ]
  openEditDialog = (element: any) => {
    this.editCategoryForm.controls['categoryName'].setValue(element.categoryName);
   
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      title: 'Edit Category',
      form: this.editCategoryForm,
      fields: this.editCategoryFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);//returns undefined
      console.log(element.categoryId, result.value);
      this.editCategory(element.categoryId, result.value)
    });

  }
  editCategory(id: number, changedData: any) {
    this.data.editCategoryDetails(id, changedData).subscribe({
      next: () => {
        this.snackBar.open(`Category Id ${id} Was Edited Succesfully`);  
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }
  deleteCategory = (row: any) => {
    console.log(row.doctorId);
    this.data.deleteCategory(row.categoryId).subscribe({
      next: () => {
        this.snackBar.open('Category Was Deleted Succesfully');
      },
      error:()=>{

      },complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }

    });

  }
  buttons=[
   
    {'name':'Delete',action:this.deleteCategory}
  ]
}
