import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { PlayersComponent } from './components/players/players.component';

import { CategoriesComponent } from './components/categories/categories.component';
import { ProductsComponent } from './components/products/products.component';
import { RequestsComponent } from './components/requests/requests.component';
import { GameRequestsComponent } from './components/requests/game-requests/game-requests.component';
import { RedeemRequestsComponent } from './components/requests/redeem-requests/redeem-requests.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';



@NgModule({
  declarations: [
   AdminDashboardComponent,
   PlayersComponent,
  
   CategoriesComponent,
   ProductsComponent,
   RequestsComponent,
   GameRequestsComponent,
   RedeemRequestsComponent,
   LeaderboardComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    FormsModule,
    HttpClientModule
  ],
  providers:[
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}}
  ]
})
export class AdminModule { }
