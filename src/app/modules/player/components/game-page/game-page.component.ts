import { identifierName } from '@angular/compiler';
import { Component, OnChanges, OnInit, Pipe, PipeTransform, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatExpansionPanelTitle } from '@angular/material/expansion';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { DialogBoxComponent } from 'src/app/modules/shared/components/dialog-box/dialog-box.component';
import { IQuestion, IResult } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})
export class GamePageComponent implements OnInit,OnChanges {

  constructor(
    private router: Router,
     private route: ActivatedRoute,
      public dialog: MatDialog,
      private playerData:PlayerDataService
    ) { }

    gameId!:any;
    playerId:any;
    questionsSet!:any;
  ngOnInit(): void {
     if(localStorage.getItem('id')!==null){
      this.playerId=localStorage.getItem('id')
     }
    this.route.params.subscribe((params: Params) => {
      console.log(params)
      this.gameId = params['gameId'];
      console.log(this.gameId)
    })
   
     
  this.playerData.getQuestions(this.gameId).subscribe({
    next:(response:any)=>{
      this.questionsSet=response;
      console.log(this.questionsSet);
      
    },
    error:(error)=>{
      console.log(error.message);
    },
    complete:()=>{
      this.questions=this.questionsSet;
      console.log(this.questions.length);
      this.qns=this.questions.length;
    }
  })
  // for(let index=0;index<this.questionsSet.length;index++){
  //   this.questions.push(this.questionsSet[index]);
  // }
  // console.log(this.questions);
}



  max_mistakes = 5;
  remainingMistakes: number = this.max_mistakes;
  wordArray: string[] = []
  succes = false;
  gameOver: boolean = false;

  countDown!: Subscription;
  counter = 0;
  tick = 1000;
  // questions!:any;
  

  ngOnChanges(changes: SimpleChanges): void {
 
    if (
      changes?.['question']?.currentValue &&
      changes?.['question'].currentValue !== changes?.['question'].previousValue
    ) {
      this.remainingMistakes = this.max_mistakes;
      this.succes = false;
    }


  }



  wrongGuess(letter: string) {
    const match = this.word.match(new RegExp(letter, 'gi'));
    return match ? 0 : 1;
  }
  count = 0;
  qns:number=5;


  questions = [
    { hintName: 'The Man Who Works for salary', wordName: 'Employee' },
    { hintName: 'The Man Who ', wordName: 'Employee' },
    { hintName: ' Who Works for salary', wordName: 'Employee' },
    { hintName: 'TheWorks for salary', wordName: 'Employee' },
    { hintName: 'The for salary', wordName: 'Employee' }
    // ...this.questionsSet
  ]
  index = 0;
  category = 'Industry'
  score=0;
  finalScore=0;
  question = this.questions[this.index].hintName;
  word = this.questions[this.index].wordName;
  guesses: string[] = []
  rightGuesses: string[] = [];
  
 
  started = false;
  solved = false;

  next() {
    this.guesses = [];
    this.rightGuesses=[];
    if (this.index === this.qns) {
      this.gameOver = true;
      this.exitGame();
      console.log("Game over");
    }
    if (this.gameOver === false) {
      console.log(this.index)
      this.pickAquestion();
    }
    
    this.count++
  }
  seconds!: number;
  totalMiliseconds!: number;
  exitGame() {
    this.seconds = this.counter;
    this.totalMiliseconds = this.seconds * 1000;
    console.log(this.totalMiliseconds);
    this.countDown.unsubscribe();
    console.log(this.counter)
    const finalScore=this.calculateScore()
    this.openResultDialog(finalScore);
    const result:IResult={
      "gameDuration":this.totalMiliseconds,
      "score":finalScore,
      "tournamentId":this.gameId,
      "playerId":parseInt(this.playerId)
    }
    console.log(result);
    this.addToLeaderBoard(result)
    
  
  }
  addToLeaderBoard(result:IResult){
     this.playerData.addDataToLeaderboard(result).subscribe();
  }
  calculateScore(){
    const finalScore:number=parseFloat(((this.score*100)/this.qns).toFixed(2));
    console.log(finalScore)
    console.log(this.finalScore)
    return finalScore
  }
  pickAquestion() {
    this.remainingMistakes = this.max_mistakes;

    if (this.gameOver === false) {
      this.question = this.questions[this.index].hintName;
      this.word = this.questions[this.index].wordName;
      console.log(this.word);
      this.index++;
      console.log(this.question);
      for (let index = 0; index < this.word.length; index++) {
        const char = this.word[index];
        this.rightGuesses[index] = '_';
        
      }
    }

  }

  startTimer() {
    this.countDown = timer(0, this.tick).subscribe((count) => {
      
      ++this.counter;
    });
  }
  startGame() {
    this.started = true;
    this.gameOver = false;
    this.count = 1;
    this.pickAquestion()
    this.startTimer();
  }
  guess(letter: string) {
    if (this.guesses.includes(letter)) {
      return;
    }
    this.guesses = [...this.guesses, letter];
    console.log(this.guesses);
    this.checkletter(letter);
  }

  checkletter(letter: string) {
    this.remainingMistakes = this.remainingMistakes - this.wrongGuess(letter);

    for (let i = 0; i < this.word.length; i++) {
      if (this.word[i].toLowerCase() === letter.toLowerCase()) {
        console.log('true');
        this.rightGuesses[i] = letter.toLowerCase();
        console.log(this.rightGuesses);
      }
      console.log(this.remainingMistakes);
    }
    this.isSolved();
  }
  isSolved() {
    if (!this.hasSpace(this.rightGuesses) || this.remainingMistakes === 0) {
      if(!this.hasSpace(this.rightGuesses)){
        this.score++;
        console.log("score",this.score);
      
      }
      if (this.index === this.qns) {
        this.gameOver = true;
        this.exitGame();
        console.log("Game over");
      }
      this.solved = true;
      setTimeout(() => {
        this.rightGuesses=[];
        this.next();
      }, 500);
    }


  }

  hasSpace(array: string[]) {
    for (let index = 0; index < array.length; index++) {
      if (array[index] === '_') {
        return true;
      }
    }

    return false;
  }


  openResultDialog = (score:number) => {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.panelClass = 'bg-color';

    dialogConfig.data = {

      title: 'Results',
      message: 'Game Over!',
      time: this.counter,
      score: score,
      action2: 'View LeaderBoard',
      
    };

    const dialogRef = this.dialog.open(DialogBoxComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.router.navigate(['leaderboard',{tournamentId: this.gameId}],{
        relativeTo:this.route,
      })
    });

  }
}


