import { Component, OnInit } from '@angular/core';
import { ITournament } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss']
})
export class TournamentDetailsComponent implements OnInit {

  constructor(private playerData:PlayerDataService) { }

  tournament!:ITournament[]
  ngOnInit(): void {
    this.playerData.getAllTournaments(null).subscribe({
      next:(response:ITournament[])=>{
        this.tournament=response;
        console.log(this.tournament);
      },
      error:(error)=>{
        console.log(error.message);
      }
    })
  }


}
