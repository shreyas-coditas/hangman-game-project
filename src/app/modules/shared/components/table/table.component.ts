import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor() { }
  @Input() dataSource!: any;
  @Input() dataColumns!: string[];
  @Input() buttons: any;
  ngOnInit(): void {
  }

}
