import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { IGift } from 'src/app/types';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(
    private data:DataService,
    private formBuilder:FormBuilder,
    private dialog:MatDialog,
    private snackBar:MatSnackBar
    ) { }
  giftsList!:IGift[]
  giftColumns: string[] = ['giftId', 'giftName', 'giftValue','actions']
  ngOnInit(): void {
    this.data.getGifts().subscribe({
      next:(response:IGift[])=>{
        this.giftsList=response;
      },
      error:(error)=>{
        console.log(error.message);
      }
    })
  }



  addGiftForm = this.formBuilder.group({
    giftName: ['', Validators.required],
    giftValue: ['', Validators.required]
  });


  addGiftFormDetails = [
    { label: "Gift Name", inputType: "text", placeholder: "Enter The Name", value: "", controlName: "giftName" },
    { label: "Gift Price", inputType: "text", placeholder: "Enter The Price", value: "", controlName: "giftValue" }
  ]

  openAddGiftDialog = () => {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
     
      title: 'Add Category',
      form: this.addGiftForm,
      fields: this.addGiftFormDetails,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      
      
      this.addNewGift(result.value)
      console.log(result);
    });

  }

  addNewGift(details:any){
    this.data.addGift(details).subscribe({
      next:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      },
      
    });
  }


  editGiftForm = this.formBuilder.group({
    giftId: ['', Validators.required],
    giftValue: ['', Validators.required]
  });


  editGiftFormFileds = [
    { label: "Gift Id", inputType: "text", placeholder: "", value: "", controlName: "giftId" },
    { label: "Gift Name", inputType: "text", placeholder: "", value: "", controlName: "giftValue" }
  ]
  openEditDialog = (element: any) => {
    this.editGiftForm.controls['giftId'].setValue(element.giftId);
   
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      title: 'Edit Gift',
      form: this.editGiftForm,
      fields: this.editGiftFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);//returns undefined
      // console.log(element.giftId, result.value);
      this.editGift(element.giftId, result.value)
    });

  }
  editGift(id: number, changedData: any) {
    const data={
      "giftId":id,
      "giftValue":changedData
    }
    this.data.editGift(id, changedData).subscribe({
      next: () => {
        this.snackBar.open(`Gift Id ${id} Was Edited Succesfully`);  
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }
  deleteGift = (row: any) => {
    console.log(row.doctorId);
    this.data.deleteGift(row.giftId).subscribe({
      next: () => {
        this.snackBar.open('Gift Was Deleted Succesfully');
      },
      error:()=>{

      },complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }

    });

  }
  buttons=[
    // {'name':'Edit',action:this.openEditDialog},
    {'name':'Delete',action:this.deleteGift}
  ]
}

