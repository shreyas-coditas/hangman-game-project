import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  tokenObj!: any;
  canActivate(
    route: ActivatedRouteSnapshot,
  ){
    
    if (localStorage.getItem('role') === route.data['role']) {
      return true;
    }
    return false;
  }
  
}
