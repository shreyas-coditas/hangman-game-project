import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'hangman-game';

  constructor(private formBuilder:FormBuilder){
    
  }
formgroup!:FormGroup;
  fn(){
    this.formgroup = this.formBuilder.group({
      email: [''],
      name: [''],
    })
  }

  ngOnInit(): void {
    const form = this.formBuilder.group({
      name: [''],
      github: [''],
      website: [''],
      server: [''],
      communications: [[]]
    });
  }
}
