import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllTournamentsComponent } from './components/all-tournaments/all-tournaments.component';
import { CreateTournamentComponent } from './components/create-tournament/create-tournament.component';
import { GamePageComponent } from './components/game-page/game-page.component';

import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { MyTournamentsComponent } from './components/my-tournaments/my-tournaments.component';
import { PlayedTournamentsComponent } from './components/played-tournaments/played-tournaments.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TournamentDetailsComponent } from './components/tournament-details/tournament-details.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path:'',component:HomeComponent,children:[
    {path:'all-tournaments',component:AllTournamentsComponent},
    {path:'played-tournaments',component:PlayedTournamentsComponent},
    {path:'my-tournaments',component:MyTournamentsComponent},
    {path:'profile',component:ProfileComponent},
    {path:'create-game',component:CreateTournamentComponent},
    {path:'game/:gameId',component:GamePageComponent,children:[ 
      { path:'leaderboard',component:LeaderboardComponent}
    ]}
  ]},
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule { }
