import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  @Input() label!: string;
  @Output() onClick = new EventEmitter();
  @Input() color='warn';
  @Input() type:string = "button";
  @Input() isDisabled=false;
    onClickButton(event:Event) {
      this.onClick.emit(event);
    }
}
