import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }
  @Input() formGroup!: FormGroup;
  @Input() formFields!: any;
  @Output() inputEmmiter = new EventEmitter();
  ngOnInit(): void {
  }
  onSubmit() {
    console.log(this.editDoctorForm);
  }
  editDoctorForm = this.formBuilder.group({
    doctorName: [''],
    specialization: [''],
  });

  passInput(event: Event) {
    this.inputEmmiter.emit(event);
  }
  editDoctorFormFileds = [
    { label: "Name", inputType: "text", placeholder: "Enter Docotor Name", value: "", controlName: "doctorName" },
    { label: "Specialization", inputType: "text", placeholder: "Enter The Specialization", value: "", controlName: "specialization" },
  ]
}
