import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ModalFormComponent } from '../../shared/components/modal-form/modal-form.component';
import { PlayerDataService } from '../services/player-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private formBuilder:FormBuilder,
    private playerService:PlayerDataService,
    private snackBar:MatSnackBar,
    private router:Router
    ) { }

  ngOnInit(): void {
  }
  showFiller = false;

  resetPasswordForm = this.formBuilder.group({
    userName: ['', [Validators.required, Validators.email]],
    oldPassword: ['', Validators.required],
    newPassword: ['', Validators.required],
  });


  resetPasswordFormFileds = [
    { label: "Enter Username/Email", inputType: "email", placeholder: "Enter The Email", value: "", controlName: "userName" },
    { label: "Old Password", inputType: "password", placeholder: "Enter Old Password", value: "", controlName: "oldPassword" },
    { label: "New Password", inputType: "password", placeholder: "Enter New Password", value: "", controlName: "newPassword" },
  ]

  openResetPasswordDialog = () => {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Reset Password',
      form: this.resetPasswordForm,
      fields: this.resetPasswordFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      
      this.resetYourPassword(result.value);
      this.snackBar.open("Your PassWord Has Been Reset");
      console.log(result);
    });

  }

  resetYourPassword(changes:any){
    this.playerService.resetPassword(changes).subscribe({
      next:()=>{
        // this.snackBar.open("Your PassWord Has Been Reset");
      }
    });
  }

  currentSeconds!:number;
  logOut(){

    this.currentSeconds=Date.now()
    this.playerService.logOutTime(this.currentSeconds).subscribe();
    localStorage.setItem('token','');
    localStorage.setItem('role','');
    localStorage.setItem('id','');
    this.router.navigate(['']);

  }
}
