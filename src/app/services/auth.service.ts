import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ILogin, ISignUp } from '../types';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { }

  headers={
    "ngrok-skip-browser-warning":"1234", 
  }

  proceedSignUp(signUpCredentials:any){
    return this.http.post('api/signUp',signUpCredentials)
  }
  proceedLogin(loginCredentials:any){
    return this.http.post('api/signin',loginCredentials)

  }
  
  authenticate(loginCredentials:ILogin){
    return this.http.post('/login',loginCredentials,{'headers':this.headers})
  }

  isLoggedIn(){
    if(localStorage.getItem('token')){
      return true;
    }
    else{
      return false;
    }
  }

  forgotPassword(credetials:any){
    return this.http.post('api/credentials/forgotPassword',credetials,{'headers':this.headers})
  }
  resetPassword(credetials:any){
    return this.http.post('api/credentials/resetPassword',credetials,{'headers':this.headers})
  }
  getOtp(credentials:any){
    return this.http.post('api/forgot',credentials)
  }
  verifyOtp(credentials:any){
    return this.http.post('api/verifyOtp',credentials)
  }
}
