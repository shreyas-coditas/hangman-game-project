import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appDisable]'
})
export class DisableDirective implements OnInit{

  constructor( private element: ElementRef,) { }

  ngOnInit(): void {
    // if (this.permission === 'NONE') {
    //   this.element.nativeElement.disabled = true;
    // }
  }

}
