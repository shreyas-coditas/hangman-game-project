import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IRedeemRequest } from 'src/app/types';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-redeem-requests',
  templateUrl: './redeem-requests.component.html',
  styleUrls: ['./redeem-requests.component.scss']
})
export class RedeemRequestsComponent implements OnInit {

  constructor(private data:DataService,private snackBar:MatSnackBar) { }

  pendingRequests!:IRedeemRequest[];
  ngOnInit(): void {
    this.data.getGiftRequests().subscribe({
      next:(response)=>{
        this.pendingRequests=response;
        console.log(this.pendingRequests)
      }
    })



    
  }

  acceptRedeemRequest(requestId:number){
    const details={
      redeemRequestId:requestId,
      status:"APPROVED"
    }
    this.data.acceseptRedeemRequest(details).subscribe({
      next:()=>{
        this.snackBar.open("Request Approved");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }
  rejectRedeemRequest(requestId:number){
    const details={
      redeemRequestId:requestId,
      status:"REJECTED"
    }
    this.data.acceseptRedeemRequest(details).subscribe({
      next:()=>{
        this.snackBar.open("Request Rejected");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }

 

}
