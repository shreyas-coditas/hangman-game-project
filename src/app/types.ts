export interface ILogin{
    "userName":string,
    "userPassword":string
}
export interface ISignUp{
    "userName":string,
    "userPassword":string,
    "playerName":string
}
export interface IAuthResponse{
    accessToken:string,
    tokenRole:string,
    tokenUserID:number
}
export interface IPlayer{
    "playerId": number,
    "playerName": string,
    "lastLoginTime": string|null,
    "gamesPlayed": number,
    "totalCoins": number,
    "userName": string,
    "userPassword": string,
    "userId": number
}

export interface ICategory{
    "categoryId": number,
    "categoryName": string,
    "tournamentList": string[]|null,
    "deleted": boolean
}

export interface ITournament{
    "tournamentId": number,
    "tournamentName": string,
    "firstPriceCoins": number,
    "secondPriceCoins": number,
    "thirdPriceCoins": number,
    "tournamentStatus": string,
    "categoryList": string[]|null,
    "wordDtoList": any
}
export interface IQuestion{
    "wordName": string,
    "hintName": string
}

export interface IProfile{
    "playerId": number,
  "playerName": string,
  "lastLoginTime": any,
  "gamesPlayed": number,
  "totalCoins": number,
  "userName": string,
  "userPassword": string,
  "userId": number,
  "noOfTournamentsCreated": number
}

export interface IGift{
    "giftId": number,
    "giftName": string,
    "giftValue": number,
    "redeemRequestList": string[]
}

export interface IResult{
    "gameDuration":number,
     "score":number,
     "tournamentId":number,
     "playerId":number
}

export interface IprogressData{
    
    tournamentName:string[],
    gameDuration:number[]
}
export interface IPlayedTournaments{
    "score": number,
    "gameDuration": number,
    "tournamentName": string,
    "playerName": string,
    "tournamentId": number,
    "playerId": number
}
export interface IRedeemRequest{
    "redeemRequestId": number,
    "redeemRequestStatus": string,
    "playerId": number,
    "playerName": string,
    "giftName": string,
    "giftId": number
}
export interface ILeaderBoard{
    gameDuration:number,
    playTime:number,
    playerId:6
    playerName:string,
    score: 100,
    tournamentId: 2
    tournamentName:string
}