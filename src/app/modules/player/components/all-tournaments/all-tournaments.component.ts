import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ICategory, ITournament } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';

@Component({
  selector: 'app-all-tournaments',
  templateUrl: './all-tournaments.component.html',
  styleUrls: ['./all-tournaments.component.scss']
})
export class AllTournamentsComponent implements OnInit {

  constructor(
    private playerData: PlayerDataService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  categories!: ICategory[]
  tournaments!: ITournament[]
  selectedCategoriesList = new FormControl([]);
  sortByList!: string[]
  sortItem: string='tournamentId';
  categoryString: string = 'default';
  sortOrder: string = 'asc';
  pageSize: number = 5;
  pages = [3, 5, 10, 15, 20, 25]
  pageNum:number=0;
  ngOnInit(): void {
    this.playerData.getAllTournaments(null).subscribe({
      next: (response) => {
        this.tournaments = response;
        console.log(this.tournaments);

        this.sortByList = [...Object.keys(this.tournaments[0])]
        console.log(this.sortByList)
      },
      error: (error) => {
        console.log(error.message);
      }
    })
    // this.fetchBySpecificity()
    this.playerData.getCategories().subscribe({
      next: (response) => {
        this.categories = response;
        console.log(this.categories);
      }
    })

  }
  goToGamePage(gameId: any) {
    // localStorage.setItem('gameId',gameId)
    this.router.navigate(['player', 'game', gameId])
    console.log(gameId)
  }
  selectChange(event: any) {
    console.log(event.value);
    this.sortItem = event.value;
    this.fetchBySpecificity()
  }
  selectPageSize(event: any) {
    this.pageSize = event.value;
    this.fetchBySpecificity()
  }
  selectFilterCategory(event: any) {
    console.log(event.value)
    this.categoryString = event.value.join(',');
    console.log(this.categoryString)
    this.fetchBySpecificity()
  }
  dscendingOrder() {
    this.sortOrder = 'desc'

    this.fetchBySpecificity()
  }
  ascendingOrder() {
    this.sortOrder = 'asc'

    this.fetchBySpecificity()
  }
  previous(){
    if(this.pageNum>0){
      this.pageNum--;
      this.fetchBySpecificity()
    }
  }
  nextPage(){
    this.pageNum++;
    this.fetchBySpecificity()
  }
  fetchBySpecificity() {
    const specifications = {
      categories: this.categoryString,
      pageNum:this.pageNum,
      pageSize: this.pageSize,
      sortBy: this.sortItem,
      sortOrder: this.sortOrder
    }
    this.playerData.getAllTournaments(specifications).subscribe({
      next: (response) => {
        this.tournaments = response;
        console.log(this.tournaments);
      },
      error: (error) => {
        console.log(error.message);
      }
    })
  }

}
