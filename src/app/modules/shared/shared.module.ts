import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TableComponent } from './components/table/table.component';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { FormComponent } from './components/form/form.component';
import { ReactiveFormsModule } from '@angular/forms';

import { ModalFormComponent } from './components/modal-form/modal-form.component';
import { ButtonComponent } from './components/button/button.component';

import { MatPaginatorModule } from '@angular/material/paginator';
import { InputComponent } from './components/input/input.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CardComponent } from './components/card/card.component';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { FormatTimePipe } from './pipes/format-time.pipe';




const MaterialComponents = [
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatBadgeModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatListModule,
  MatDividerModule,
  MatGridListModule,
  MatExpansionModule,
  MatCardModule,
  MatTabsModule,
  MatStepperModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTooltipModule,
  MatDatepickerModule,
  MatSnackBarModule,
  MatDialogModule,
  MatTableModule,
  MatPaginatorModule
]

@NgModule({
  declarations: [ButtonComponent,InputComponent,TableComponent,ModalFormComponent,FormComponent,SidebarComponent, NavbarComponent, CardComponent, DialogBoxComponent,FormatTimePipe],
  imports: [
    CommonModule,
    MaterialComponents,
    ReactiveFormsModule,
  ],
  entryComponents:[ModalFormComponent],
  exports:[MaterialComponents,ButtonComponent,InputComponent,TableComponent,ModalFormComponent,FormComponent,SidebarComponent,NavbarComponent,CardComponent,DialogBoxComponent,FormatTimePipe]
})
export class SharedModule { }
