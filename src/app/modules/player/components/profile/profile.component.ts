import { Component, ElementRef, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Chart, registerables } from 'chart.js/auto';
import * as Highcharts from 'highcharts';
import { IGift, IProfile, IprogressData, ITournament } from 'src/app/types';
import { PlayerDataService } from '../../services/player-data.service';
Chart.register(...registerables)

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  progressData!: IprogressData
  playerId!: number;
  allGifts!: IGift[];
  availableGifts!: any;
  unavailableGifts!: any;
  coins!: number;
  constructor(
    private elementRef: ElementRef,
    private playerData: PlayerDataService,
    private snackBar: MatSnackBar
  ) { }
  profileDetails!: IProfile;
  ngOnInit(): void {
    this.playerData.getProfileDetails().subscribe({
      next: (response: IProfile) => {
        this.profileDetails = response;

      },
      error: (error) => {
        console.log(error.message)
      },
      complete: () => {
        this.coins = this.profileDetails.totalCoins;
        this.playerId = this.profileDetails.playerId;
        this.getGifts();
      }
    })


    this.playerData.getProgress().subscribe({
      next: (response: IprogressData) => {
        this.progressData = response;
        console.log(response)
        console.log(this.progressData)
      }, error: (error) => {
        console.log(error.message)
      },
      complete: () => {
        this.createChart(this.progressData)
      }

    })

  }

  getGifts() {
    this.playerData.getAllGifts().subscribe({
      next: (response) => {
        this.allGifts = response;
        console.log(this.allGifts)
      },
      error: () => { },
      complete: () => {
        console.log(this.coins)
        this.filterGifts(this.allGifts)
      }

    })
  }

  filterGifts(gifts: IGift[]) {

    this.availableGifts = gifts.filter((gift: any) => {
      if (gift.giftValue <= this.coins) {
        return gift;
      }
    })
    this.unavailableGifts = gifts.filter((gift: any) => {
      if (gift.giftValue > this.coins) {
        return gift;
      }
    })
    console.log(this.availableGifts)
  }
  redeemGift(giftId: number) {
    const giftDetail = {
      "playerId": this.playerId,
      "giftId": giftId
    }
    this.playerData.redeemMygift(giftDetail).subscribe({
      next: () => { },
      error: () => { },
      complete: () => {
        this.snackBar.open("Redeem Request Has Been Sent")
      }

    });
  }
  tournamentNames!: string[];
  durations!: number[];





  chartOptions!: Highcharts.Options;
  Highcharts: typeof Highcharts = Highcharts;
  graphData!: IprogressData;
  createChart(data: any) {

    this.graphData = data;
    this.chartOptions = {
      chart: {
        type: 'column'
      },
      xAxis: {
        categories: this.graphData.tournamentName,
        title: {
          text: 'Tournaments Played'
        },
      },
      yAxis: {
        title: {
          text: 'Time (mm:ss)'
        },
        type: 'datetime',
        labels: {
          format: '{value:%M:%S}',
        }
      },
      series: [{
        data: this.graphData.gameDuration,
        type: 'line'
      }]

    };
  }






  progressOpen = false;
  giftsOpen = false;
  toggleProgress() {
    this.progressOpen = !this.progressOpen;
  }
  toggleGifts() {
    this.giftsOpen = !this.giftsOpen;
  }
}
